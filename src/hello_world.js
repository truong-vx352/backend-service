const { exec } = require("child_process");
var ip = require("ip");

amqp.connect("amqp://localhost", function (error0, connection) {
	if (error0) {
		throw error0;
	}
	connection.createChannel(function (error1, channel) {
		if (error1) {
			throw error1;
		}
		var exchange = "exchange";

		channel.assertExchange(exchange, "direct", {
			durable: false,
		});

		channel.assertQueue(
			"",
			{
				exclusive: true,
			},
			function (error2, q) {
				if (error2) {
					throw error2;
				}
				console.log(" [*] Waiting for logs. To exit press CTRL+C");
				// channel.bindQueue(q.queue, exchange, "192.168.56.111");
				channel.bindQueue(q.queue, exchange, ip.address());

				channel.consume(
					q.queue,
					function (msg) {
						const message = JSON.parse(msg.content.toString());
						if (message?.executeType === "SHUTDOWN") exec("shutdown /s");
					},
					{
						noAck: true,
					}
				);
			}
		);
	});
});
